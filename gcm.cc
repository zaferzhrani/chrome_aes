/** @file gcm.cpp
 *
 * Author : Dhafer
 * Date   : 8/31/2022
 * Time   : 11:24 PM
 */

#include <cstring>
#include <iostream>
#include "base/logging.h"
#include "cu_aes.h"

AES_GCM::AES_GCM(const uint8_t* key,
                 uint32_t keyLen,
                 const uint8_t* iv,
                 uint32_t ivLen) {
  switch (keyLen) {
    case AES256_KEY_LEN:
      this->Nk = 8;
      this->Nr = 14;
      break;

    case AES192_KEY_LEN:
      this->Nk = 6;
      this->Nr = 12;
      break;

    case AES128_KEY_LEN:
      this->Nk = 4;
      this->Nr = 10;
      break;
  }

  this->keyLen = keyLen;
  this->ivLen = ivLen;

  memcpy(this->key, key, keyLen);
  memcpy(this->iv, iv, ivLen);

}

#define WPA_GET_BE32(a)                                      \
  ((((uint32_t)(a)[0]) << 24) | (((uint32_t)(a)[1]) << 16) | \
   (((uint32_t)(a)[2]) << 8) | ((uint32_t)(a)[3]))

#define WPA_PUT_BE32(a, val)                              \
  do {                                                    \
    (a)[0] = (uint8_t)((((uint32_t)(val)) >> 24) & 0xff); \
    (a)[1] = (uint8_t)((((uint32_t)(val)) >> 16) & 0xff); \
    (a)[2] = (uint8_t)((((uint32_t)(val)) >> 8) & 0xff);  \
    (a)[3] = (uint8_t)(((uint32_t)(val)) & 0xff);         \
  } while (0)

#define WPA_PUT_BE64(a, val)                      \
  do {                                            \
    (a)[0] = (uint8_t)(((uint64_t)(val)) >> 56);  \
    (a)[1] = (uint8_t)(((uint64_t)(val)) >> 48);  \
    (a)[2] = (uint8_t)(((uint64_t)(val)) >> 40);  \
    (a)[3] = (uint8_t)(((uint64_t)(val)) >> 32);  \
    (a)[4] = (uint8_t)(((uint64_t)(val)) >> 24);  \
    (a)[5] = (uint8_t)(((uint64_t)(val)) >> 16);  \
    (a)[6] = (uint8_t)(((uint64_t)(val)) >> 8);   \
    (a)[7] = (uint8_t)(((uint64_t)(val)) & 0xff); \
  } while (0)

#define BIT(x) (1 << (x))

void AES_GCM::inc32(uint8_t* block) {
  uint32_t val;
  val = WPA_GET_BE32(block + AES_BLOCKLEN - 4);
  val++;
  WPA_PUT_BE32(block + AES_BLOCKLEN - 4, val);
}

void AES_GCM::xor_block(uint8_t* dst, const uint8_t* src) {
  uint32_t* d = (uint32_t*)dst;
  uint32_t* s = (uint32_t*)src;
  *d++ ^= *s++;
  *d++ ^= *s++;
  *d++ ^= *s++;
  *d++ ^= *s++;
}

void AES_GCM::shift_right_block(uint8_t* v) {
  uint32_t val;

  val = WPA_GET_BE32(v + 12);
  val >>= 1;
  if (v[11] & 0x01)
    val |= 0x80000000;
  WPA_PUT_BE32(v + 12, val);

  val = WPA_GET_BE32(v + 8);
  val >>= 1;
  if (v[7] & 0x01)
    val |= 0x80000000;
  WPA_PUT_BE32(v + 8, val);

  val = WPA_GET_BE32(v + 4);
  val >>= 1;
  if (v[3] & 0x01)
    val |= 0x80000000;
  WPA_PUT_BE32(v + 4, val);

  val = WPA_GET_BE32(v);
  val >>= 1;
  WPA_PUT_BE32(v, val);
}

/* Multiplication in GF(2^128) */
void AES_GCM::GF_mult(const uint8_t* x, const uint8_t* y, uint8_t* z) {
  uint8_t v[AES_BLOCKLEN];
  int i, j;

  memset(z, 0, AES_BLOCKLEN); /* Z_0 = 0^128 */
  memcpy(v, y, AES_BLOCKLEN); /* V_0 = Y */

  for (i = 0; i < AES_BLOCKLEN; i++) {
    for (j = 0; j < 8; j++) {
      if (x[i] & BIT(7 - j)) {
        /* Z_(i + 1) = Z_i XOR V_i */
        xor_block(z, v);
      } else {
        /* Z_(i + 1) = Z_i */
      }

      if (v[15] & 0x01) {
        /* V_(i + 1) = (V_i >> 1) XOR R */
        shift_right_block(v);
        /* R = 11100001 || 0^120 */
        v[0] ^= 0xe1;
      } else {
        /* V_(i + 1) = V_i >> 1 */
        shift_right_block(v);
      }
    }
  }
}

void AES_GCM::G_hash(const uint8_t* h,
                     const uint8_t* x,
                     uint32_t xlen,
                     uint8_t* y) {
  uint32_t m, i;
  const uint8_t* xpos = x;
  uint8_t tmp[AES_BLOCKLEN];

  m = xlen / AES_BLOCKLEN;

  for (i = 0; i < m; i++) {
    /* Y_i = (Y^(i-1) XOR X_i) dot H */
    xor_block(y, xpos);
    xpos += AES_BLOCKLEN;

    /* dot operation:
     * multiplication operation for binary Galois (finite) field of
     * 2^128 elements */
    GF_mult(y, h, tmp);
    memcpy(y, tmp, AES_BLOCKLEN);
  }

  if (x + xlen > xpos) {
    /* Add zero padded last block */
    uint32_t last = x + xlen - xpos;
    memcpy(tmp, xpos, last);
    memset(tmp + last, 0, sizeof(tmp) - last);

    /* Y_i = (Y^(i-1) XOR X_i) dot H */
    xor_block(y, tmp);

    /* dot operation:
     * multiplication operation for binary Galois (finite) field of
     * 2^128 elements */
    GF_mult(y, h, tmp);
    memcpy(y, tmp, 16);
  }

  /* Return Y_m */
}

void AES_GCM::AES_GCTR(const uint8_t* icb,
                       const uint8_t* x,
                       uint32_t xlen,
                       uint8_t* y) {
  uint32_t i, n, last;
  uint8_t cb[AES_BLOCKLEN], tmp[AES_BLOCKLEN];
  const uint8_t* xpos = x;
  uint8_t* ypos = y;

  struct AES_ctx aesCtx;
  AES_init_key_iv(&aesCtx, key, iv);

  n = xlen / AES_BLOCKLEN;

  memcpy(cb, icb, AES_BLOCKLEN);
  /* Full blocks */
  for (i = 0; i < n; i++) {
    AES_encrypt(&aesCtx, cb, ypos);
    xor_block(ypos, xpos);
    xpos += AES_BLOCKLEN;
    ypos += AES_BLOCKLEN;
    inc32(cb);
  }

  last = x + xlen - xpos;
  if (last) {
    /* Last, partial block */
    AES_encrypt(&aesCtx, cb, tmp);
    for (i = 0; i < last; i++)
      *ypos++ = *xpos++ ^ tmp[i];
  }
}

void AES_GCM::aes_gcm_init_hash_subkey(uint8_t* hash) {
  AES_ctx_t ctx;
  AES_init_key_iv(&ctx, key, iv);

  /* Generate hash subkey H = AES_K(0^128) */
  memset(hash, 0, AES_BLOCKLEN);
  AES_encrypt(&ctx, hash, hash);
}

void AES_GCM::GCM_prepare_j0(const uint8_t* iv,
                             uint32_t iv_len,
                             const uint8_t* hash,
                             uint8_t* J0) {
  uint8_t len_buf[16];

  if (iv_len == 12) {
    /* Prepare block J_0 = IV || 0^31 || 1 [len(IV) = 96] */
    memcpy(J0, iv, iv_len);
    memset(J0 + iv_len, 0, AES_BLOCKLEN - iv_len);
    J0[AES_BLOCKLEN - 1] = 0x01;
  } else {
    /*
     * s = 128 * ceil(len(IV)/128) - len(IV)
     * J_0 = GHASH_H(IV || 0^(s+64) || [len(IV)]_64)
     */
    memset(J0, 0, AES_BLOCKLEN);
    G_hash(hash, iv, iv_len, J0);
    WPA_PUT_BE64(len_buf, 0);
    WPA_PUT_BE64(len_buf + 8, iv_len * 8);
    G_hash(hash, len_buf, sizeof(len_buf), J0);
  }
}

void AES_GCM::AES_GCM_GCTR(const uint8_t* J0,
                           const uint8_t* in,
                           uint32_t len,
                           uint8_t* out) {
  uint8_t J0inc[AES_BLOCKLEN];

  memcpy(J0inc, J0, AES_BLOCKLEN);
  inc32(J0inc);

  AES_GCTR(J0inc, in, len, out);
}

void AES_GCM::GCM_Ghash(const uint8_t* hash,
                        const uint8_t* AAD,
                        uint32_t AAD_len,
                        const uint8_t* cipher,
                        uint32_t cipher_len,
                        uint8_t* single_block) {
  uint8_t len_buf[AES_BLOCKLEN];
  /*
   * u = 128 * ceil[len(C)/128] - len(C)
   * v = 128 * ceil[len(A)/128] - len(A)
   * S = GHASH_H(A || 0^v || C || 0^u || [len(A)]64 || [len(C)]64)
   * (i.e., zero padded to block size A || C and lengths of each in bits)
   */
  memset(single_block, 0, AES_BLOCKLEN);
  G_hash(hash, AAD, AAD_len, single_block);
  G_hash(hash, cipher, cipher_len, single_block);
  WPA_PUT_BE64(len_buf, AAD_len * 8);
  WPA_PUT_BE64(len_buf + 8, cipher_len * 8);
  G_hash(hash, len_buf, sizeof(len_buf), single_block);
}

/**
 * AES_GCM_encrypt - GCM-AE_K(IV, P, A)
 */
bool AES_GCM::AES_GCM_encrypt(const uint8_t* plain,
                              uint32_t plainLen,
                              const uint8_t* add,
                              uint32_t aadLen,
                              uint8_t* cipherBuf,
                              uint8_t* tag) {
  uint8_t hash[AES_BLOCKLEN];
  uint8_t J0[AES_BLOCKLEN];  // pre-counter block
  uint8_t S[AES_BLOCKLEN];   // single output block

  aes_gcm_init_hash_subkey(hash);

  GCM_prepare_j0(iv, ivLen, hash, J0);

  /* C = GCTR_K(inc_32(J_0), P) */
  AES_GCM_GCTR(J0, plain, plainLen, cipherBuf);

  GCM_Ghash(hash, add, aadLen, cipherBuf, plainLen, S);

  /* T = MSB_t(GCTR_K(J_0, S)) */
  AES_GCTR(J0, S, sizeof(S), tag);

  return true;
}

/**
 * AES_GCM_decrypt - GCM-AD_K(IV, C, A, T)
 */
bool AES_GCM::AES_GCM_decrypt(const uint8_t* cipher,
                              uint32_t cipherLen,
                              const uint8_t* add,
                              uint32_t aadLen,
                              const uint8_t* tag,
                              uint8_t* plainBuf) {

  uint8_t hash[AES_BLOCKLEN], tag_calculated[AES_BLOCKLEN];
  uint8_t J0[AES_BLOCKLEN];  // pre-counter block
  uint8_t S[AES_BLOCKLEN];   // single output block

  aes_gcm_init_hash_subkey(hash);

  GCM_prepare_j0(iv, ivLen, hash, J0);

  /* P = GCTR_K(inc_32(J_0), C) */
  AES_GCM_GCTR(J0, cipher, cipherLen, plainBuf);

  GCM_Ghash(hash, add, aadLen, cipher, cipherLen, S);

  /* tag_calculated' = MSB_t(GCTR_K(J_0, S)) */
  AES_GCTR(J0, S, sizeof(S), tag_calculated);
  // FIXME:to test
  // memset(plainBuf, 0, cipherLen);
  // printf("DEC plainLen=%d\n",cipherLen);

  if (memcmp(tag, tag_calculated, AES_BLOCKLEN) != 0) {
    // DLOG(INFO) << "GCM Tag mismatch\n";
    return false;
  }
  // DLOG(INFO) << "\nGCM Tag True";

  return true;
}
