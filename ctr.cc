/** @file ctr.cpp
 *
 * Author : Dhafer
 * Date   : 8/31/2022
 * Time   : 11:24 PM
 */

#include <cstring>
#include <iostream>
#include "cu_aes.h"

AES_CTR::AES_CTR(const uint8_t* key,
                 uint32_t keyLen,
                 const uint8_t* iv,
                 uint32_t ivLen) {
  switch (keyLen) {
    case AES256_KEY_LEN:
      this->Nk = 8;
      this->Nr = 14;
      break;

    case AES192_KEY_LEN:
      this->Nk = 6;
      this->Nr = 12;
      break;

    case AES128_KEY_LEN:
      this->Nk = 4;
      this->Nr = 10;
      break;
  }

  this->keyLen = keyLen;
  this->ivLen = ivLen;

  memcpy(this->key, key, keyLen);
  memcpy(this->iv, iv, ivLen);

}

void AES_CTR::AES_CTR_encrypt(uint8_t* plain,
                              uint32_t plainLen,
                              uint8_t* cipherBuf) {
  uint8_t block[AES_BLOCKLEN];
  AES_ctx_t ctx;
  AES_init_key_iv(&ctx, key, iv);

  memcpy(cipherBuf, plain, plainLen); /* copy plain buf to working buf. */

  uint32_t cipherLen =
      plainLen; /*cipher len same plain len in ctr mode (No padding required)*/

  uint32_t i, bi;
  for (i = 0, bi = AES_BLOCKLEN; i < cipherLen; ++i, ++bi) {
    if (bi == AES_BLOCKLEN) /* we need to regen xor compliment in buffer */
    {
      memcpy(block, ctx.Iv, AES_BLOCKLEN);
      AES_encrypt(&ctx, block, block);

      /* Increment Iv and handle overflow */
      for (bi = (AES_BLOCKLEN - 1); bi >= 0; --bi) {
        /* inc will overflow */
        if (ctx.Iv[bi] == 255) {
          ctx.Iv[bi] = 0;
          continue;
        }
        ctx.Iv[bi] += 1;
        break;
      }
      bi = 0;
    }

    cipherBuf[i] = (cipherBuf[i] ^ block[bi]);
  }
}

void AES_CTR::AES_CTR_decrypt(uint8_t* cipher,
                              uint32_t cipherLen,
                              uint8_t* plainBuf) {
  uint8_t block[AES_BLOCKLEN];

  AES_ctx_t ctx;
  AES_init_key_iv(&ctx, key, iv);

  memcpy(plainBuf, cipher, cipherLen); /* copy cipher buf to working buf. */

  uint32_t i, bi;

  for (i = 0, bi = AES_BLOCKLEN; i < cipherLen; ++i, ++bi) {
    if (bi == AES_BLOCKLEN) /* we need to regen xor compliment in buffer */
    {
      memcpy(block, ctx.Iv, AES_BLOCKLEN);
      AES_encrypt(&ctx, block, block);

      /* Increment Iv and handle overflow */
      for (bi = (AES_BLOCKLEN - 1); bi >= 0; --bi) {
        /* inc will overflow */
        if (ctx.Iv[bi] == 255) {
          ctx.Iv[bi] = 0;
          continue;
        }
        ctx.Iv[bi] += 1;
        break;
      }
      bi = 0;
    }

    plainBuf[i] = (plainBuf[i] ^ block[bi]);
  }
}