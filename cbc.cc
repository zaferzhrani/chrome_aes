/** @file cbc.cpp
 *
 * Author : Dhafer
 * Date   : 09/06/2022
 * Time   : 09:32 PM
 */

#include <cstring>
#include <iostream>
#include "base/logging.h"
#include "cu_aes.h"

AES_CBC::AES_CBC(const uint8_t* key,
                 uint32_t keyLen,
                 const uint8_t* iv,
                 uint32_t ivLen) {
  switch (keyLen) {
    case AES256_KEY_LEN:
      this->Nk = 8;
      this->Nr = 14;
      break;

    case AES192_KEY_LEN:
      this->Nk = 6;
      this->Nr = 12;
      break;

    case AES128_KEY_LEN:
      this->Nk = 4;
      this->Nr = 10;
      break;
  }

  this->keyLen = keyLen;
  this->ivLen = ivLen;

  memcpy(this->key, key, keyLen);
  memcpy(this->iv, iv, ivLen);

}

uint32_t AES_CBC::AES_CBC_encrypt(const uint8_t* plain,
                                  uint32_t plainLen,
                                  uint8_t* cipherBuf,
                                  uint32_t cipherBufSize) {
  EncCheckLen(plainLen, cipherBufSize);

  memcpy(cipherBuf, plain, plainLen); /* copy plain buf to working buf. */

  uint32_t cipherLen = add_padding(cipherBuf, plainLen);

  AES_ctx_t ctx;
  AES_init_key_iv(&ctx, key, iv);
  uint8_t* Iv = iv;

  for (uint32_t i = 0; i < cipherLen; i += AES_BLOCKLEN) {
    XorWithIv(cipherBuf, Iv);
    AES_encrypt(&ctx, cipherBuf, cipherBuf);  // encrypt Block
    Iv = cipherBuf;
    cipherBuf += AES_BLOCKLEN;
  }
  memset(cipherBuf, 0, cipherBufSize - cipherLen);

  return cipherLen;
}

uint32_t AES_CBC::AES_CBC_decrypt(const uint8_t* cipher,
                                  uint32_t cipherLen,
                                  uint8_t* plainBuf,
                                  uint32_t plainBufSize) {
  DecCheckLen(plainBufSize, cipherLen);

  memcpy(plainBuf, cipher, cipherLen); /* copy cipher buf to working buf. */

  AES_ctx_t ctx;
  AES_init_key_iv(&ctx, key, iv);
  uint8_t storeNextIv[AES_BLOCKLEN];
  uint32_t i;
  for (i = 0; i < cipherLen; i += AES_BLOCKLEN) {
    memcpy(storeNextIv, plainBuf, AES_BLOCKLEN);
    AES_decrypt(&ctx, plainBuf, plainBuf);  // decrypt Block
    XorWithIv(plainBuf, ctx.Iv);
    memcpy(ctx.Iv, storeNextIv, AES_BLOCKLEN);
    plainBuf += AES_BLOCKLEN; /*shift pointer by AES_BLOCKLEN */
  }

  plainBuf -= cipherLen; /*re-shift pointer*/

  for (size_t i = 0; i < cipherLen; i++) {
    printf("%.2X ", plainBuf[i]);
  }

  uint32_t actualCipherLen = remove_padding(plainBuf, cipherLen);
  if (actualCipherLen == 0) {
    DLOG(INFO) << "\nCBC failed Decryption";
  }

  return actualCipherLen;
}
